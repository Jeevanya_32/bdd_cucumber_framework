package Data_Driven_Stepdef;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import API_common_methods.common_method_handle_API;
import common_methods_utility.Directory_handle;
import common_methods_utility.handle_API_logs;
import endpoint_package.Patch_APIendpoint;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;

public class patch_stepdef {
	String endpoint;
	String requestbody;
	String responsebody;
	int statuscode;
	File log_dirname;
	
	@Given("enter patch {string} and {string} in request body parameters")
	public void enter_patch_and_in_request_body_parameters(String req_name, String req_job) {
	log_dirname=Directory_handle.log_directory_creation("API_patchlogs");
	endpoint=Patch_APIendpoint.endpoint_patch();
	requestbody= "{\r\n"
			+ "    \"name\": \""+req_name+"\",\r\n"
			+ "    \"job\": \""+req_job+"\"\r\n"
			+ "}" ;
	    //throw new io.cucumber.java.PendingException();
	}
	@When("send the patch request with payload with input data")
	public void send_the_patch_request_with_payload_with_input_data() {
	    statuscode=common_method_handle_API.patch_statuscode(requestbody, endpoint);
	    System.out.println("Statuscode is validated "+statuscode);
	    responsebody=common_method_handle_API.patch_responsebody(requestbody, endpoint);
	    System.out.println(responsebody);
	   // throw new io.cucumber.java.PendingException();
	}
	@Then("validate data driven of patch status code")
	public void validate_data_driven_of_patch_status_code() {
		Assert.assertEquals(statuscode,200);
		System.out.println("Statuscode is validated");
	    //throw new io.cucumber.java.PendingException();
	}
	@Then("validate data driven of patch response body parameters")
	public void validate_data_driven_of_patch_response_body_parameters() throws IOException {
		handle_API_logs.evidence_creator(log_dirname,"testclass_patch",endpoint, requestbody, responsebody);
	    JsonPath jsp_req=new JsonPath(requestbody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		
		JsonPath jsp_res=new JsonPath(responsebody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		Assert.assertEquals(res_name,req_name);
		Assert.assertEquals(res_job,req_job);
		System.out.println("patch responsebody validation successful");
	   // throw new io.cucumber.java.PendingException();
	}
}
