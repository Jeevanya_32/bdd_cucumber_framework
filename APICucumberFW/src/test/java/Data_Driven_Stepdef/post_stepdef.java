package Data_Driven_Stepdef;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import API_common_methods.common_method_handle_API;
import apirequest_repository.PostAPI_request_repository;
import common_methods_utility.Directory_handle;
import common_methods_utility.handle_API_logs;
import endpoint_package.Post_APIendpoint;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;

public class post_stepdef {
	String requestbody;
	String endpoint;
    int statuscode;
	String responsebody;
	File log_dirname;
	@Given("enter post {string} and {string} in request body parameters")
	public void enter_post_and_in_request_body_parameters(String req_name, String req_job) throws IOException {
		 log_dirname=Directory_handle.log_directory_creation("API_postlogs");
			endpoint=Post_APIendpoint.endpoint_post();
			requestbody="{\r\n"
		            + "    \"name\": \""+req_name+"\",\r\n"
		            + "    \"job\": \""+req_job+"\"\r\n" + "}";
	   // throw new io.cucumber.java.PendingException();
	}
	@When("send the post request with payload with input data")
	public void send_the_post_request_with_payload_with_input_data() {
		statuscode =common_method_handle_API.post_statuscode(requestbody, endpoint); 
		System.out.println("Post API statuscode is "+statuscode);
        responsebody = common_method_handle_API.post_responsebody(requestbody, endpoint);
        System.out.println(responsebody);
	    //throw new io.cucumber.java.PendingException();
	}
	@Then("validate data driven of post status code")
	public void validate_data_driven_of_post_status_code() {
		Assert.assertEquals(statuscode, 201);
		System.out.println("statuscode is validated");
	   //throw new io.cucumber.java.PendingException();
	}
	@Then("validate data driven of post response body parameters")
	public void validate_data_driven_of_post_response_body_parameters() throws IOException {
		 handle_API_logs.evidence_creator(log_dirname,"testclass_post",endpoint, requestbody, responsebody);
			JsonPath jsp_req=new JsonPath(requestbody);
			String req_name = jsp_req.getString("name");
			String req_job = jsp_req.getString("job");
			
			JsonPath jsp_res=new JsonPath(responsebody);
			String res_name = jsp_res.getString("name");
			String res_job = jsp_res.getString("job");
			Assert.assertEquals(res_name,req_name);
			Assert.assertEquals(res_job,req_job);
			System.out.println("responsebody validated successfully");
	    //throw new io.cucumber.java.PendingException();
	}
}
