Feature: Trigger patch API

Scenario Outline: Trigger the patch API request with  request parameters
  Given enter patch "<Name>" and "<Job>" in request body parameters
  When send the patch request with payload with input data
  Then validate data driven of patch status code 
  And validate data driven of patch response body parameters 
  
 Examples:
         |Name|Job|
         |Jhon|SrQA|
         |Kavi|SrMgr|
         