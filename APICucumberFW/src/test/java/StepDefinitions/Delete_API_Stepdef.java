package StepDefinitions;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import API_common_methods.common_method_handle_API;
import common_methods_utility.Directory_handle;
import common_methods_utility.handle_API_logs;
import endpoint_package.Delete_APIendpoint;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;


public class Delete_API_Stepdef {
	String endpoint;
	int statuscode;
	File log_dirname;
	@Given("enter delete endpoint")
	public void enter_delete_endpoint() {
		 log_dirname=Directory_handle.log_directory_creation("API_deletelogs");
		 endpoint=Delete_APIendpoint.endpoint_delete();
	   // throw new io.cucumber.java.PendingException();
	}
	@When("send the delete request")
	public void send_the_delete_request() {
		statuscode =common_method_handle_API.delete_statuscode(endpoint); 
	    //throw new io.cucumber.java.PendingException();
	}
	@Then("validate delete status code")
	public void validate_delete_status_code() throws IOException {
		 Assert.assertEquals(statuscode,204);
		System.out.println("Delete status code is validated");
	    //throw new io.cucumber.java.PendingException();
	}
}
