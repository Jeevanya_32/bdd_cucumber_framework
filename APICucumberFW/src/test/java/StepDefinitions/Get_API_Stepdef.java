package StepDefinitions;

import java.io.File;
import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;

import API_common_methods.common_method_handle_API;
import common_methods_utility.Directory_handle;
import common_methods_utility.handle_API_logs;
import endpoint_package.Get_APIendpoint;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;

public class Get_API_Stepdef {
	String endpoint;
	int statuscode;
	String responsebody;
	File log_dirname;
	
	@Given("enter get endpoint")
	public void enter_get_endpoint() {
		 log_dirname=Directory_handle.log_directory_creation("API_getlogs");
		 endpoint=Get_APIendpoint.endpoint_get();
	   // throw new io.cucumber.java.PendingException();
	}
	@When("send the get request")
	public void send_the_get_request() {
		statuscode =common_method_handle_API.get_statuscode(endpoint); 
		responsebody=common_method_handle_API.get_responsebody(endpoint);
	    //throw new io.cucumber.java.PendingException();
	}
	@Then("validate get status code")
	public void validate_get_status_code() {
		Assert.assertEquals(statuscode,200);
	}
	@Then("validate get response body")
	public void validate_get_response_body() throws IOException {
		 handle_API_logs.evidence_creator(log_dirname,"testclass_get",endpoint,responsebody);
			int expected_id[]={1,2,3,4,5,6};
		    String expected_email[]={"george.bluth@reqres.in", "janet.weaver@reqres.in",
		                             "emma.wong@reqres.in","eve.holt@reqres.in",
		                             "charles.morris@reqres.in","tracey.ramos@reqres.in"};
		    String expected_first_name[]= {"George","Janet","Emma","Eve","Charles","Tracey"};
		    String expected_last_name[]= {"Bluth","Weaver","Wong","Holt","Morris","Ramos"};

	        JsonPath jsp_res=new JsonPath(responsebody);
			JSONObject array_res=new JSONObject(responsebody);
			JSONArray dataarray = array_res.getJSONArray("data");
			int count=dataarray.length();
			
		    for (int i=0; i<count; i++) {
				int res_id=dataarray.getJSONObject(i).getInt("id");
				String res_email=dataarray.getJSONObject(i).getString("email");
				String res_firstname=dataarray.getJSONObject(i).getString("first_name");
				String res_lastname=dataarray.getJSONObject(i).getString("last_name");
				
			    int exp_id=expected_id[i];
			    String exp_email=expected_email[i];
			    String exp_firstname=expected_first_name[i];
			    String exp_lastname=expected_last_name[i];
		    }
	System.out.println("Get response body validation successful");
	//throw new io.cucumber.java.PendingException();
			 
}
}