package StepDefinitions;

import static io.restassured.RestAssured.given;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import API_common_methods.common_method_handle_API;
import apirequest_repository.PostAPI_request_repository;
import common_methods_utility.Directory_handle;
import common_methods_utility.handle_API_logs;
import endpoint_package.Post_APIendpoint;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;


public class Post_API_Stepdef{
	String requestbody;
	String endpoint;
    int statuscode;
	String responsebody;
	File log_dirname;
	//Tagged Hooks
	@Before("@PostAPI_Testcase or @DeleteAPI_Testcase")
	public void setup() {
		System.out.println("Triggering process begins");
	}
	@Given("enter post NAME and JOB in request body")
	public void enter_name_and_job_in_request_body() throws IOException {
		 log_dirname=Directory_handle.log_directory_creation("API_postlogs");
		endpoint=Post_APIendpoint.endpoint_post();
		requestbody=PostAPI_request_repository.post_req();
	    // Write code here that turns the phrase above into concrete actions
	    //throw new io.cucumber.java.PendingException();
	}
	@When("send the post request with payload")
	public void send_the_post_request_with_payload() {
		
		statuscode =common_method_handle_API.post_statuscode(requestbody, endpoint); 
		System.out.println("Post API statuscode is "+statuscode);
        responsebody = common_method_handle_API.post_responsebody(requestbody, endpoint);
        System.out.println(responsebody);
	    //throw new io.cucumber.java.PendingException();
	}
	@Then("validate post status code")
	public void validate_post_status_code() {
		Assert.assertEquals(statuscode, 201);
		System.out.println("statuscode is validated");
	   
	    //throw new io.cucumber.java.PendingException();
	}
	@Then("validate post response body parameters")
	public void validate_post_response_body_parameters() throws IOException {
		 handle_API_logs.evidence_creator(log_dirname,"testclass_post",endpoint, requestbody, responsebody);
		JsonPath jsp_req=new JsonPath(requestbody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		
		JsonPath jsp_res=new JsonPath(responsebody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		Assert.assertEquals(res_name,req_name);
		Assert.assertEquals(res_job,req_job);
		System.out.println("responsebody validation successful");
	   // throw new io.cucumber.java.PendingException();
	}
     //Hooks
	@After("@PostAPI_Testcase or @DeleteAPI_Testcase")
	public void teardown() {
		System.out.println("Process ended");
	}

}
