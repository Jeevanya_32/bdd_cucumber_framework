package StepDefinitions;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import API_common_methods.common_method_handle_API;
import apirequest_repository.PutAPI_request_repository;
import common_methods_utility.Directory_handle;
import common_methods_utility.handle_API_logs;
import endpoint_package.Put_APIendpoint;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;

public class Put_API_Stepdef{
	String requestbody;
	String endpoint;
    int statuscode;
	String responsebody;
	 File log_dirname;
@Given("enter NAME and JOB in put request body")
public void enter_name_and_job_in_put_request_body() throws IOException {
	 log_dirname=Directory_handle.log_directory_creation("API_putlogs");
	endpoint=Put_APIendpoint.endpoint_put();
	requestbody=PutAPI_request_repository.put_req();
    //throw new io.cucumber.java.PendingException();
}
@When("send the put request with payload")
public void send_the_put_request_with_payload() {
	statuscode =common_method_handle_API.put_statuscode(requestbody, endpoint); 
	System.out.println("Put API statuscode is "+statuscode);
    responsebody = common_method_handle_API.put_responsebody(requestbody, endpoint);
    System.out.println(responsebody);
   // throw new io.cucumber.java.PendingException();
}
@Then("validate put status code")
public void validate_put_status_code() {
	Assert.assertEquals(statuscode, 200);
	System.out.println("put statuscode is validated");
    //throw new io.cucumber.java.PendingException();
}
@Then("validate put response body parameters")
public void validate_put_response_body_parameters() throws IOException {
	 handle_API_logs.evidence_creator(log_dirname,"testclass_put",endpoint, requestbody, responsebody);
	JsonPath jsp_req=new JsonPath(requestbody);
	String req_name = jsp_req.getString("name");
	String req_job = jsp_req.getString("job");
	
	JsonPath jsp_res=new JsonPath(responsebody);
	String res_name = jsp_res.getString("name");
	String res_job = jsp_res.getString("job");
	Assert.assertEquals(res_name,req_name);
	Assert.assertEquals(res_job,req_job);
	System.out.println("put responsebody validation successful");
   // throw new io.cucumber.java.PendingException();
}


}
