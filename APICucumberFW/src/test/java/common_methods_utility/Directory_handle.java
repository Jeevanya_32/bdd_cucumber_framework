package common_methods_utility;
import java.io.File;
public class Directory_handle {
public static File log_directory_creation(String log_dirname) {
	//fetch current project directory
	String project_directory=System.getProperty("user.dir");
	System.out.println("Current project directory path is "+project_directory);
	
	File directory_name=new File(project_directory +"\\API_log\\" +log_dirname);
	
	if (directory_name.exists()) {
		directory_name.delete();
		System.out.println(directory_name +" is deleted");
		directory_name.mkdir();
		System.out.println(directory_name+" is created");
	}
	else{
		directory_name.mkdir();
		System.out.println(directory_name +" is created");
	}
	     return directory_name;
}
}
