package common_methods_utility;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class handle_API_logs {
	public static void evidence_creator(File log_dirname,String File_name,String endpoint,String requestbody,String responsebody)throws IOException 
	{
		File newFile=new File(log_dirname+"\\"+File_name+".txt");
		System.out.println(newFile.getName()+" is created for saving request and responsebody");
 
		FileWriter datawriter=new FileWriter(newFile);
		datawriter.write("Endpoint is "+endpoint +"\n\n");
		datawriter.write("Requestbody is "+requestbody +"\n\n");
		datawriter.write("Responsebody is "+responsebody);
		datawriter.close();
 }
	
	public static void evidence_creator(File log_dirname,String File_name,String endpoint,String responsebody)throws IOException 
 {
		File newFile=new File(log_dirname+"\\"+File_name+".txt");
		System.out.println(newFile.getName()+" is created for saving responsebody");
	 
		FileWriter datawriter=new FileWriter(newFile);
	    datawriter.write("Endpoint is "+endpoint +"\n\n");
	    datawriter.write("Responsebody is "+responsebody);
	    datawriter.close();
	 }
}