package cucumber.Options;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/java/Data_Driven_features",glue={"Data_Driven_Stepdef"})
public class Data_Driven_TestRunner {

}
