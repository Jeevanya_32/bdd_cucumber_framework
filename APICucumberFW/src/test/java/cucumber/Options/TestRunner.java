package cucumber.Options;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/java/features",glue={"StepDefinitions"})
//tags= "@PostAPI_Testcase or @PutAPI_Testcase or @GetAPI_Testcase")
public class TestRunner {

}
