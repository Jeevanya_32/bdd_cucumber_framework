Feature: Trigger post API

Scenario Outline: Trigger the post API request with  request parameters
  Given enter post "<Name>" and "<Job>" in request body parameters
  When send the post request with payload with input data
  Then validate data driven of post status code 
  And validate data driven of post response body parameters 
  
 Examples:
         |Name|Job|
         |Jeevanya|QA|
         |Sandy|Mgr|
         |Dheera|SrQA|