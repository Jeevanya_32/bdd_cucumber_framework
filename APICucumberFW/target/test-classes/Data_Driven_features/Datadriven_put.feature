Feature: Trigger put API

Scenario Outline: Trigger the put API request with  request parameters
  Given enter put "<Name>" and "<Job>" in request body parameters
  When send the put request with payload with input data
  Then validate data driven of put status code 
  And validate data driven of put response body parameters 
  
 Examples:
         |Name|Job|
         |Jaya|QA|
         |Mahi|Mgr|
         