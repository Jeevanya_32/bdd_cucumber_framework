Feature: Trigger delete API
@DeleteAPI_Testcase
Scenario: Trigger the delete API 
  Given enter delete endpoint
  When send the delete request
  Then validate delete status code
  