Feature: Trigger get API
@GetAPI_Testcase
Scenario: Trigger the get API endpoint
  Given enter get endpoint
  When send the get request
  Then validate get status code
  And validate get response body