Feature: Trigger Patch API
@PatchAPI_Testcase
Scenario: Trigger the patch API request with valid request parameters
  Given enter NAME and JOB in patch request body
  When send the patch request with payload
  Then validate patch status code
  And validate patch response body parameters