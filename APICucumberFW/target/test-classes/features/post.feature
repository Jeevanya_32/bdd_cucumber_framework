Feature: Trigger post API
@PostAPI_Testcase
Scenario: Trigger the post API request with valid request parameters
  Given enter post NAME and JOB in request body
  When send the post request with payload
  Then validate post status code
  And validate post response body parameters