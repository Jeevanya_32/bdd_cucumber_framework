Feature: Trigger Put API
@PutAPI_Testcase
Scenario: Trigger the put API request with valid request parameters
  Given enter NAME and JOB in put request body
  When send the put request with payload
  Then validate put status code
  And validate put response body parameters