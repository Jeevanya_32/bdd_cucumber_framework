  **Behaviour Driven Development(BDD)**

•	BDD is a software development methodology to develop software.

•	It helps to cascade information between all stakeholders by easily understandable language and formatting standards.

•	To standardize the automation and to ease our work we need a framework.

  BDD  consists of,

**1)Language(Gherkin)**  - it’s a plain text contains in the file extension ‘.feature’ with the keywords, 

o	Feature

o	Scenario & Scenario Outline - Given,when,Then,And

o	Examples

o	@ Tags

**2)Format/standard(Cucumber Framework)** - defines the standard of BDD and maps the functional java code with plain text.

Structure of  framework

**1.Test Driving Mechanism :**

Using the annotations of @CucumberOptions and @RunWith and importing it.

a)In TestRunner class,we give the location of the feature file and stepdefinition file package name in attributes of features and glue repectively.And for the execution control we can add the @ Tags.

It executes the feature file and coordinate the steps defined in the feature files with stepdefinitions.


b)By adding **Maven surefire Plugin** in dependencies, we can run our test cases in cmd.


**2.Feature file** - consists of description of our tests using the keywords.

**3.TestClass:**

In API Package, For each API’s  I have separate testclass and multiple testcases for each testclass.

**4.Common method components:**

For configuring and triggering the API using
Given(), When() and Then()

**i) API components:**

Request repository,endpoint and common functons to trigger API and extract statuscode and responsebody

**ii)Utility function Package:**


a)To get test log files, we are creating directory,if it is not exists.

If directory exists,we are deleting and creating it.

b) To create logfile and endpoint,requestbody,reponsebody and its respective headers added into notepad file of each testclass



c)Excel data extractor is to read the test datas from an excel file

**5.Step Definition file**

Translates the testcase steps from feature file into code and defines the operation to be executed for each step of test scenario.

It maps the plain text steps in feature file to corresponding code.

We are calling the respective methods of requestbody,endpoint,responsebody and logfiles creation.

**HOOKS** is added to structure the testcases and perform certain certain activity before and after execution of testcases.(@Before and @After is used)

**6.Test Data** 

**a)Excel file:**

It consists of  requestbody data entered in excel file.

**b)Scenario Outline and Examples in feature file:**

For **parameterization**,to specify more than one input data we use pipe( | ) symbol in feature file using keyword ‘Examples’.

**7.Libraries:**

Below given are the dependencies added and handled by Maven repository

Junit – To write,organise and execute repeatable automated tests 

Cucumber-Creates wrapper on functional code and map with plain text

RestAssured – To trigger API and extract statuscode and responsebody

TestNG – To validate the reponse body using Assert class, organise and  run testcases through testNG

JsonPath – To parse the response

Apache.poi – To read data from an excel file








